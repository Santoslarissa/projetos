
public class Horario0 {
	
	int segundo;
	
	public Horario0() {
		setHora((byte)0);
		setMinuto((byte)0);
		setSegundo((byte)0);
	}
	
	public Horario0 (byte hora, byte minuto, byte segundo){
		setHora(hora);
		setMinuto(minuto);
		setSegundo(segundo);
	}
	

	void setHora (byte h) {
		if  ( h  >=  0  &&  h  <=  23 )  {
			segundo  = h  *  3600 ;
		}
	}
	
	int getHora () {
		int h;
		h= (segundo/3600);
		return h;
	}
	
	void setMinuto (byte m) {
		
		if (m>=0 && m<=59) {
			segundo  = m * 60;
		}

	}
	
	int getMinuto () {
		int m, h;
		h= getHora();
		m= (segundo - h * 3600)/60;
		return m;

	}
	
	void setSegundo (byte s) {
		if (s>=0 && s<=59) {
			segundo = segundo + s;
		}

	}
	
	int getSegundo () {
		int h,m,s;
		h= getHora();
		m= getMinuto();
		s= (segundo - h * 3600);
		s= (s - m * 60);
		
		return s;
 
	}

 
	public String toString() {
		return getHora() + ":" + getMinuto() + ":" + getSegundo();
	}	

	
	
	public void incrementaSegundo() {
		
		int s = (segundo + 1);
		
		if(s == 86400) {
			segundo = 0;
		}else {
			segundo = s;
		}

	}
	
	public void incrementaMinuto() {
		
		if(segundo/60 == 1440) {
			segundo = 0;
			incrementaHora();
		}else {
			  segundo = segundo + 60;
		}

	}

	public void incrementaHora() {
		 
		if(segundo/3600 == 24) {
			segundo = 0 ;
		}else {
				segundo = segundo + 3600;
		}
	}
	
}
