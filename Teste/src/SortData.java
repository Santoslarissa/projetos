import java.util.Comparator;

public class SortData implements Comparator<Data> {

	@Override
	public int compare(Data o1, Data o2) {
		if(o1.menor(o2)) return -1;
		if(o1.equals(o2)) return 0;
		return 1;
	}

}