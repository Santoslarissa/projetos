import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestListRelogio {
	public static void main(String[] args) {
		
		Horario hra = new Horario(23, 59 , 0);
		Horario hrb = new Horario(23, 59 , 0);
		Horario hrc = new Horario(23, 59 , 0);
		
		Data dta = new Data(17, 5, 2021);
		Data dtb = new Data(16, 6, 2021);
		Data dtc = new Data(16, 5, 2020);
		
		Relogio ra = new Relogio(hra, dta) ;
		Relogio rb = new Relogio(hrb, dtb);
		Relogio rc = new Relogio(hrc, dtc);
		
		List<Relogio> Relogios;
		
		Relogios = new ArrayList<Relogio>();
		
		Relogios.add(rb);

		Relogios.add(rc);

		Relogios.add(ra);				
		
		for(int i = 0; i < Relogios.size(); i++) {
			System.out.println(i + "->" + Relogios.get(i));
		}
		
		Collections.sort(Relogios, new SortRelogio());
	
		System.out.println("--------------------------------");
		for(int i = 0; i < Relogios.size(); i++) {
			System.out.println(i + "->" + Relogios.get(i));
		}
	}
}