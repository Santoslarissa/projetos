
public interface IData {
	byte getDia() ;

	void setDia(byte dia);

	byte getMes() ;

	void setMes(byte mes);

	 short getAno() ;

	void setAno(short ano) ;
	
	String toString() ;
	
	void incrementaDia() ;
	
	void incrementaMes() ;

	void incrementaAno() ;

    void incrementaDia(int n);
    
    void incrementaMes(int n) ;
    
    void incrementaAno(int n) ;

}
