import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestListD {
	public static void main(String[] args) {
		
		Data a = new Data(16,6,2021);
		
		Data b = new Data(17,06,2020);
		
		Data c = new Data(17,06,2019);
		
		List<Data> Datas;
		
		Datas = new ArrayList<Data>();
		
		Datas.add(b);

		Datas.add(c);

		Datas.add(a);				
		
		for(int i = 0; i < Datas.size(); i++) {
			System.out.println(i + "->" + Datas.get(i));
		}
		
		Collections.sort(Datas, new SortData());
	
		System.out.println("--------------------------------");
		for(int i = 0; i < Datas.size(); i++) {
			System.out.println(i + "->" + Datas.get(i));
		}
	}
}
