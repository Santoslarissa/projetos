import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestListDNG {
	public static void main(String[] args) {
		
		DataNG a = new DataNG(16,6,2021);
		
		DataNG b = new DataNG(17,06,2020);
		
		DataNG c = new DataNG(17,06,2019);
		
		List<DataNG> Datas;
		
		Datas = new ArrayList<DataNG>();
		
		Datas.add(b);

		Datas.add(c);

		Datas.add(a);				
		
		for(int i = 0; i < Datas.size(); i++) {
			System.out.println(i + "->" + Datas.get(i));
		}
		
		Collections.sort(Datas, new SortDataNG());
	
		System.out.println("--------------------------------");
		for(int i = 0; i < Datas.size(); i++) {
			System.out.println(i + "->" + Datas.get(i));
		}
	}
}
