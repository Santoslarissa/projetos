import java.util.Comparator;

public class SortDataNG implements Comparator<DataNG> {

	@Override
	public int compare(DataNG o1, DataNG o2) {
		if(o1.menor(o2)) return -1;
		if(o1.equals(o2)) return 0;
		return 1;
	}

}