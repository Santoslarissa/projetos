import java.util.Comparator;

public class SortHorarioNG implements Comparator<HorarioNG> {

	@Override
	public int compare(HorarioNG o1, HorarioNG o2) {
		if(o1.menor(o2)) return -1;
		if(o1.equals(o2)) return 0;
		return 1;
	}

}