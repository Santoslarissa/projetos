public class Horario2 {
	int segundo;
	
	public Horario2() {
		setHora((byte)0);
		setMinuto((byte)0);
		setSegundo((byte)0);
	}
	
	public Horario2(byte h, byte m, byte s){
		setHora(h);
		setMinuto(m);
		setSegundo(s);
	}

	
	void setHora(byte h) {
		int x = h * 3600;
		if (h >= 0 && h <= 23) {
			x = x + h * 3600;
		}
	}
	
	int getHora() {
		int h;
		h= (segundo/3600);
		return h;
	}
	
	public void incrementaHora() {
		 int h =segundo + 3600;
		 
		if(h == 24) {
			segundo = 0 ;
		}else {
				segundo = h;
		}
			
	}

	void setMinuto(byte m) {
		int x = m * 60;
		if (m>=0 && m<=59) {
			x  = x + m * 60;
		}
	}
	
	int getMinuto() {
		int m, h;
		h= (segundo/3600);
		m= (segundo - h * 3600)/60;
		return m;
	}
	
	public void incrementaMinuto() {
		int m = (segundo + 60);
		
		if(m == 60) {
			segundo = 0;
			incrementaHora();
		}else {
			segundo = m;
		}

	}
	
	void setSegundo(byte s) {
		int x = s + 86399;
		if (s>=0 && s<=59) {
			x = x + s;
		}
	}
	
	int getSegundo() {
		int h,m,s;
		h= (segundo/3600);
		m= (segundo - h * 3600);
		m= (m/60);
		s= (segundo - h * 3600);
		s= (s - m * 60);
		
		return s;
	}
	
	public void incrementaSegundo() {
		int s = (segundo + 1);
		
		if(s == 86399) {
			segundo = 0;
			incrementaMinuto();
		}else {
			segundo = s;
		}

	}
	
	
	public String toString() {
		return getHora() + ":" + getMinuto() + ":" + getSegundo();
	}	
	

} 