import java.util.Comparator;

public class SortHorario implements Comparator<Horario> {

	@Override
	public int compare(Horario o1, Horario o2) {
		if(o1.menor(o2)) return -1;
		if(o1.equals(o2)) return 0;
		return 1;
	}

}
